package kr.edcan.ksh_0106;

import android.app.Service;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.io.IOException;

import cz.msebera.android.httpclient.Header;

public class MyService extends Service {
    private AsyncHttpClient client;
    private final String url = "https://dl.dropboxusercontent.com/content_link/AfNnJKdPI21hPr5d2rhnmdfk7PFJkzOeNPObNf68dKA70OM7JtF2FZrs30zTTwIH/file?dl=1";

    private String path;
    private AssetFileDescriptor ad;
    private MediaPlayer mediaPlayer;

    public MyService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                stopSelf();
            }
        });
//        client.get(url, new AsyncHttpResponseHandler() {
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//
//            }
//
//            @Override
//            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//                Toast.makeText(MyService.this, "Download failed\nError " + statusCode, Toast.LENGTH_SHORT).show();
//            }
//        });
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            mp3Play();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        mp3Stop();
        super.onDestroy();
    }

    private void mp3Play() throws IOException {
//        AssetManager assetManager = getAssets();
        ad = getAssets().openFd("test.mp3");
//        path = "file:///android_asset/test.mp3";
        if(!mediaPlayer.isPlaying()) {
            new Thread(runnable).start();
            Toast.makeText(getApplicationContext(), "재생 시작", Toast.LENGTH_SHORT).show();
        }
    }

    private void mp3Stop() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        Toast.makeText(getApplicationContext(), "재생 중지", Toast.LENGTH_SHORT).show();
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                mediaPlayer.setDataSource(ad.getFileDescriptor());
                mediaPlayer.prepare();
                mediaPlayer.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };
}
